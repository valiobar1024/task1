import React from 'react'
import logo from '../logo.svg';
import BooksData from '../helpers/BooksData'
class HomePage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            books: []
        };

        this.getBooks()
    }
    async getBooks() {
        let data = await BooksData.getBooks().then(function (response) {
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        this.setState({books: data.data});
        this.setState({isLoading: false})
    }
    render() {
        return (
            <div className='row d-flex justify-content-center'>
                {
                this.state.isLoading ? (
                    <div>
                        <img src={logo}
                            className="App-logo"
                            alt="logo"/>
                    </div>
                ) : (this.state.books.map(function (item, i) {
                    return <div className='col-12 col-md-3 book-container'>
                        <div className='book'>
                            <h2>{
                                item.name
                            }</h2>
                            <ul> {
                                item.authors.map(au => {
                                    return <li>{
                                        au.name
                                    }</li>
                            })
                            } </ul>
                        </div>
                    </div>;
                }))
            } </div>
        )
    }
}
export default HomePage
