/**
 * Created by Kvaba on 3/12/2019.
 */
const passport = require('passport')
const User = require('../data/User')
const encryption = require('../tools/encryption')
const environment = require('../config/settings')
const crypto = require("crypto");
const bcrypt = require("bcrypt");


module.exports = {

  loginPost: ((req, res, next) => {
    return passport.authenticate('local-login', (err, token, userData) => {
      if (err) {
        if (err.name === 'IncorrectCredentialsError') {
          return res.status(200).json({
            success: false,
            message: err.message
          })
        }

        return res.status(200).json({
          success: false,
          message: 'Could not process the form.'
        })
      }

      return res.json({
        success: true,
        message: 'You have successfully logged in!',
        token,
        user: userData
      })
    })(req, res, next)
  }),

  

 

  
}

