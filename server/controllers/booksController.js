const booksRepository = require('../pgData/booksRepository')
const Authors = require('../data/Authors')
const mongoose = require('mongoose')

module.exports = {
   
    getBooks:async(req,res)=>{
      let books = await booksRepository.getAllBooks()    
      let booksData =[]
      for(let i =0;i<books.length;i++){
        let bookAuthors = await Authors.find({ _id: { $in: books[i].authors }}, 'name')   
        books[i].authors = bookAuthors
    }  
   
      res.json({
          success:true,
          data:books
      })
    }


}