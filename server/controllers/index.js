const users = require('./userController')
const books = require('./booksController')
const authors = require('./authorsController')
module.exports = {
  users:users,
  books:books,
  authors:authors
 
}
