const {Pool} = require('pg')
const fs = require('fs');
const settings = require('../config/settings')[process.env.NODE_ENV || 'development']
var path = require('path');


let Connection = (function () {
  let pool;

  function createPool() { 
    console.log('CREATE POOL')
    let newPool = new Pool({
      connectionString: settings.pg, 
      ssl: {
        rejectUnauthorized: false
      },    
      min: 1,
      max: 45, // max number of clients in the pool
      idleTimeoutMillis: 100, // how long a client is allowed to remain idle before being closed

    })
    newPool.on('error', (err, client) => {
      if (err) {
        console.log("POOL CONNECTION ERROR: ");
        console.log(err.stack)
        if (client) {
          console.log("RELEASING CLIENT");
         client.release(true);
        }
      } else {
        console.log("POOL CONNECTION ERROR: NO ERR");
        if (client) {
          console.log("RELEASING CLIENT");
          client.release(true);
        }
      }
      console.log('POOL ERROR: ')
      console.log(err)
    }) 
    return newPool;
  }
  return {
    getConnetion: function () {
      if (!pool) {
        pool = createPool();
      }
      return pool;
    }
  };
})();


module.exports = Connection;


