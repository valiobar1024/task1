const connection = require('./connectionsPool')
module.exports = {

    seedBooksTable: () => {
        return new Promise((resolve, reject) => {
            let query = `CREATE TABLE IF NOT EXISTS books(
            id serial PRIMARY KEY,
            name text,
            authors text[] CHECK (cardinality(authors) > 0)
        )`
            let params = []
            connection.getConnetion().connect().then(client => {
                client.query(query, params).then((res) => {
                    client.release()
                    resolve(res)
                   

                }).catch(err => {
                    client.release()
                    console.log(err)
                    reject(err)
                })
            }).catch(err => {
                console.log(err)
                reject(err)
                client.release()
            })
        })
    },
    seedBooks: (authors) => {
        return new Promise((resolve, reject) => {
            let initialBooks = []
            connection.getConnetion().connect().then(async client => {
                 let booksCount = await client.query('SELECT COUNT(*) FROM books')                
                 if(booksCount.rows[0].count>0){
                   resolve(booksCount.rows[0].count)
                   client.release()
                 }else {                
                for (let i = 0; i < 20; i++) {
                    let authorsCount = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
                    let bookAuthors = []                 
                    for (let j = 0; j < authorsCount; j++) {
                        let authorIndex = Math.floor(Math.random() * (authors.length-1));                     
                        if (bookAuthors.indexOf(authors[authorIndex]._id) < 0) 
                            bookAuthors.push(authors[authorIndex]._id.toString())
                    }
                    let params = [`Book ${i}`, bookAuthors]
                    let query = `INSERT INTO books (name, authors) VALUES($1, $2) RETURNING *;`                    
                    try {
                        let newBook = await client.query(query, params)                       
                        initialBooks.push(newBook)
                    } catch (err) {
                        console.log(err)
                        reject(err)
                        client.release()
                    }
                }              
                resolve(initialBooks.length)
                client.release()
            }
            }).catch(err => {
                console.log(err)
                reject(err)
            })

        })
    },
    getAllBooks:()=>{
        return new Promise((resolve, reject) => {
            connection.getConnetion().connect().then(async client => {
              let query = 'SELECT * FROM books'
              try{
                let allBooks =  await client.query(query) 
                resolve(allBooks.rows)
                client.release()
              }catch(err){
                reject(err)
                client.release()
              } 
            }).catch(err => {
                console.log(err)
                reject(err)
                client.release()
            })
        })
    }


    

}
