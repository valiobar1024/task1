const mongoose = require('mongoose')


const REQUIRED_VALIDATION_MESSAGE = '{PATH} is required'

let authorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: REQUIRED_VALIDATION_MESSAGE
    },
    books: {
        type: [String],
        default: []
    }
})


let Author = mongoose.model('Author', authorSchema)

module.exports = Author
module.exports.seedAuthors = () => {
    return new Promise((resolve, reject) => {
        console.log('SEED ADMIN')
        Author.find({}).then(async authors => {
            if (authors.length > 0){
                resolve(authors)
            } else {
              let initalAuthors = []
              for (let i = 0; i < 20; i++) {
                  let newAuthor = await Author.create({
                          name: `Author ${i + 1}`
                  }).catch(err => {
                      console.log(err)
                      reject(err)
                  })
                  initalAuthors.push(newAuthor)
              }
              resolve(initalAuthors)
            }           
           
        })
    })
}
