const mongoose = require('mongoose')
const encryption = require('../tools/encryption')

const REQUIRED_VALIDATION_MESSAGE = '{PATH} is required'

let userSchema = new mongoose.Schema({
  email: { type: String, required: REQUIRED_VALIDATION_MESSAGE },
  firstName: { type: String, required: REQUIRED_VALIDATION_MESSAGE },
  secondName: { type: String, required: REQUIRED_VALIDATION_MESSAGE },
  lastName: { type: String, required: REQUIRED_VALIDATION_MESSAGE },
  
  salt: String,
  hashedPass: String,
  roles: [String],
  active:{type:Boolean,default:true},
  tsRegistered:{ type: Date, default: Date.now}
})

userSchema.method({
  authenticate: function (password) {
    return encryption.generateHashedPassword(this.salt, password) === this.hashedPass  }
})


let User = mongoose.model('User', userSchema)

module.exports = User
module.exports.seedAdminUser = () => {
  console.log('SEED ADMIN')
  User.find({}).then(users => {
    if (users.length > 0) return

    let salt = encryption.generateSalt()

    let hashedPass = encryption.generateHashedPassword(salt, '123456')

    User.create({
      email: 'valio@valio.com',
      firstName: 'super',
      secondName: 'admin',
      lastName: 'user',
      phoneNumber: 359,
      salt: salt,
      hashedPass: hashedPass,
      roles: ['Admin', 'Super']
    })

  })
}
