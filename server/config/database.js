const mongoose = require('mongoose')

mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise
const User = require('../data/User')
const Authors = require('../data/Authors')
const books = require('../pgData/booksRepository')
module.exports = (settings) => {

  mongoose.connect(settings.db, { useNewUrlParser: true ,useUnifiedTopology: true})
 let db = mongoose.connection

  db.once('open', async err => {
    if (err) {
      throw err
    }
    console.log('MongoDB ready!')
   User.seedAdminUser()
   try{
    let initalAuthors = await Authors.seedAuthors()   
    await books.seedBooksTable()
    let booksCount =  await books.seedBooks(initalAuthors)
    console.log('BOOKS COUNT :'+ booksCount)
   }catch(err){
     console.log(err)
   }
  
 

  //  books.initalData()
     
    
    



  })

  db.on('error', err => console.log(`Database error: ${err}`))

}
