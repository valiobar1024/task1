const express = require('express')
 const localSignupStrategy = require('./passport/local-signup')
 const localLoginStrategy = require('./passport/local-login')
const bodyParser = require('body-parser')
const cors = require('cors')
const passport = require('passport')
const path = require('path')
const compression = require('compression')

const settings = require('./settings')


module.exports = (app) => {

  app.use(express.static(path.join(__dirname, '../../client/build')))
  app.use(express.static(path.join(__dirname, '../public')))
  app.use(passport.initialize())
  app.use(cors())
 
  passport.use('local-signup', localSignupStrategy)
  passport.use('local-login', localLoginStrategy) 
  app.use((req, res, next) => {
    if (req.user) {
      res.locals.currentUser = req.user
    }
    next()
  })

  console.log('Express ready!')
}

