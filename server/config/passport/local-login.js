const jwt = require('jsonwebtoken')
const User = require('mongoose').model('User')
const PassportLocalStrategy = require('passport-local').Strategy
const encryption = require('../../tools/encryption')

module.exports = new PassportLocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  session: false,
  passReqToCallback: true
}, (req, email, password, done) => {
  const user = {
    email: email.trim(),
    password: password.trim()
  }

  User.findOne({email: email}).then(savedUser=>{
    if (!savedUser||!savedUser.status) {
      const error = new Error('Incorrect email or password')
      error.name = 'IncorrectCredentialsError'

      return done(error)
    }

    const isMatch = savedUser.authenticate(user.password)

    if (!isMatch) {
      const error = new Error('Incorrect email or password')
      error.name = 'IncorrectCredentialsError'

      return done(error)
    }

    const payload = {
      sub: savedUser.id,
      email:savedUser.email,
      roles:savedUser.roles,
      status:savedUser.status,
      exp: Math.floor(Date.now() / 1000) + (60 * 60*24),
    }

    // create a token string
    const token = jwt.sign(payload, 's0m3 r4nd0m str1ng')
    const data = {
      firstName: savedUser.firstName,
      secondName: savedUser.secondName,
      lastName: savedUser.lastName
    }

    return done(null, token, data)

  })
})
