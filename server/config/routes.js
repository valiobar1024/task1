const auth = require('./middleware/auth-check')
const settings = require('./settings')
const bodyParser = require('body-parser')
const controllers = require('../controllers')
module.exports = (app) => {


 app.post('/api/users/login', bodyParser.json(), controllers.users.loginPost)
 
 app.get('/api/books', bodyParser.json(), controllers.books.getBooks)
 
 app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname + '/client/dist/index.html'))
  })


}

