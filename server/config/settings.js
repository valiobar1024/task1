const path = require('path')

let rootPath = path.normalize(path.join(__dirname, '/../../'))

module.exports = {
  development: {
    rootPath: rootPath,
    pg:'postgres://dwruhjpkfxvcky:bde6485be7343c9154e54bdeeee08a4e2f9cac4abe3e9db1db1786107f7f9397@ec2-54-74-156-137.eu-west-1.compute.amazonaws.com:5432/df8udki3ik27hc',  
    db: 'mongodb+srv://dbUser:279088@cluster-ekont.grvrd.mongodb.net/infoleven-tesk?retryWrites=true&w=majority',    
    port:process.env.PORT|| 1337,   
  },
  staging: {
  },
  production: {
    port: process.env.PORT,
    rootPath: rootPath,  
    pg:process.env.PORTDATABASE_URL,  
    db: process.env.MONGO_DB,    
    port:process.env.PORT,    
  }
}