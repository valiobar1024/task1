const jwt = require('jsonwebtoken')
const User = require('mongoose').model('User')

module.exports.baseAuth = (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).end()
  }

  // get the last part from a authorization header string like "bearer token-value"
  const token = req.headers.authorization.split(' ')[1]

  // decode the token using a secret key-phrase
  return jwt.verify(token, 's0m3 r4nd0m str1ng', (err, decoded) => {
    // the 401 code is for unauthorized status
    if (err) { return res.status(401).end() }

    const userId = decoded.sub

     User.findById(userId).then(user=>{
       if (!user||!user.active) {
         return res.status(401).end()
       }

       req.user = user

       return next()
     })

  })
}

module.exports.superAuth = (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).end()
  }

  // get the last part from a authorization header string like "bearer token-value"
  const token = req.headers.authorization.split(' ')[1]

  // decode the token using a secret key-phrase
   jwt.verify(token, 's0m3 r4nd0m str1ng', (err, decoded) => {
    // the 401 code is for unauthorized status
    if (err) { return res.status(401).end() }

    const userId = decoded.sub

    User.findById(userId)
      .then(user=>{

        if (!user || user.roles.indexOf('Super')<0||!user.active) {
          return res.status(401).end()
        }

        req.user = user

        return next()
      })

  })
}
